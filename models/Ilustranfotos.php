<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ilustranfotos".
 *
 * @property int $id
 * @property int $idilustran
 * @property string $fotos
 *
 * @property Ilustran $ilustran
 */
class Ilustranfotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ilustranfotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idilustran'], 'integer'],
            [['fotos'], 'string', 'max' => 150],
            [['idilustran', 'fotos'], 'unique', 'targetAttribute' => ['idilustran', 'fotos']],
            [['idilustran'], 'exist', 'skipOnError' => true, 'targetClass' => Ilustran::className(), 'targetAttribute' => ['idilustran' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idilustran' => 'Idilustran',
            'fotos' => 'Fotos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIlustran()
    {
        return $this->hasOne(Ilustran::className(), ['id' => 'idilustran']);
    }
}
