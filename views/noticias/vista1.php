<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

$this->title = $title;

?>
<div class="well well-sm"><h2 style="text-align: center;max-height: 50px;"><?= $title ?></h2></div>

<p>
        <?= Html::a('Crear Noticia', ['create'], ['class' => 'btn btn-warning']) ?>
    </p>

<div class="row">
<?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_vista1',
            'layout'=>"{summary}\n{pager}\n{items}",
        ]);
?>
</div>