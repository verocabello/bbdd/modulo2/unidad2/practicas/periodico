<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Noticiasfecha */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="noticiasfecha-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idnoticia')->textInput() ?>
       
    
    <?php
    
    echo '<label class="control-label">Fecha de publicación</label>';
    echo DatePicker::widget([
            'model'=>$model,
            'attribute' => 'fecha_publicacion',
            'options' => ['placeholder' => 'Introduzca la fecha de publicación'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' =>  'dd/mm/yyyy',
                'autoclose' => true,
            ]
        ]);
    ?>
    
    <div class="form-group">
        </br>    
    <?= Html::submitButton('Enviar', ['class' => 'btn btn-success']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
