<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ilustranfotos */

$this->title = 'Create Ilustranfotos';
$this->params['breadcrumbs'][] = ['label' => 'Ilustranfotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ilustranfotos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
