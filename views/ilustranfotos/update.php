<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ilustranfotos */

$this->title = 'Update Ilustranfotos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ilustranfotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ilustranfotos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
