<?php
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    
$this->title = "Noticias";  
?>

  <div class="col-sm-6 col-md-12">
    <div class="thumbnail noticias" style="max-height: 600px;">
      <div class="caption">
          <p style="color:darkgrey;text-align: right;"><?= implode('</p><p>',ArrayHelper::getColumn($model->noticiasfechas, 'fecha_publicacion')) ?></p>  
        <h2><?= $model->titulo ?></h2>
        <hr class="my-4">
        <p><?= $model->texto ?></p>
        <div class="btn-group btn-group-justified">
            <?= Html::a("Información del autor: ".$model->autor->nombre,['autores/viewfe','id'=>$model->idautor],['class' => ' btn btn-primary']) ?>
            <?= Html::a("Fotografías de la noticia",['autores/viewfe','id'=>$model->idautor],['class' => ' btn btn-primary']) ?>
        </div>
        
        
        <hr class="my-4">
        
        
        
        <div>
        <div style="display:inline;background:#E9E9E9;margin-bottom:4px;padding:6px;border-radius:5px;" ><?= implode('</div><div style="display:inline-block !important;margin-left:10px;background:#E9E9E9;margin-bottom:4px;padding:4px;border-radius:5px;">',ArrayHelper::getColumn($model->etiquetas, 'etiqueta')) ?></div> 
          </div>
      </div>
    </div>
  </div>



