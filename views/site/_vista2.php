<?php
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    
  


$this->title = "Autores";

//$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);
?>



<div class="autores-view">
    
 
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="caption">
        <h3><?= $model->id.", ".$model->nombre ?></h3>
    
        <p><?= "Correo electrónico: ".$model->email ?></p>
        <p><?= "Alias: ".$model->alias ?></p>
    
        <?= Html::a('Ver Noticias', ['noticias/noticiasautor','autor'=>$model->id], ['class' => 'btn btn-primary btn-md', 'style'=>'margin-top:10px;']) ?>
        
      </div>
    </div>
  </div>

    
    

    
    

   

</div>

