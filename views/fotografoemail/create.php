<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fotografoemail */

$this->title = 'Create Fotografoemail';
$this->params['breadcrumbs'][] = ['label' => 'Fotografoemails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fotografoemail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
