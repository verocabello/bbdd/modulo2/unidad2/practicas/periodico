<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ilustran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ilustran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idfotografo')->textInput() ?>

    <?= $form->field($model, 'idnoticia')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
