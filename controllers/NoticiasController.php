<?php

namespace app\controllers;

use Yii;
use app\models\Noticias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Autores;
use app\models\Fotografos;
use app\models\Etiquetas;

/**
 * NoticiasController implements the CRUD actions for Noticias model.
 */
class NoticiasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Shows frontend noticias.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $dataProvider = new ActiveDataProvider([
//            'query' => Noticias::find(),
//        ]);
//
//        return $this->render('vista1', [
//            "dataProvider" => $dataProvider,
//            "campos"=>['texto'],
//            "titulo"=>"Noticias de nuestro periódico",
//          ]);
//    }
    /**
     * Lists all Noticias models.
     * @return mixed
     */
    public function actionIndex(){
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    public function actionProbando(){
        $a=Noticias::findOne(1);
        var_dump($a);
        //var_dump($a->getCategorizans()->all());
        var_dump($a->getEtiquetas()->all()[0]->etiqueta);
        
    }
    
    public function actionNoticiasautor($autor){
        $dataProvider = new ActiveDataProvider([
            'query'=> Noticias::find()
                ->where("idautor=$autor")
        ]);
        
        $nombreautor = Autores::findOne($autor)->nombre;
        
        
        
        
        
        return $this->render('vista1', [
            "dataProvider" => $dataProvider,
            "campos"=>['texto'],
            "title"=>"Noticias del autor: $nombreautor",
          ]);
        
    }
    
    public function actionNoticiasetiqueta($etiqueta){
        $dataProvider = new ActiveDataProvider([
            'query'=> Noticias::find()
                ->innerJoinWith("etiquetas")
                ->where("etiquetas.id=$etiqueta")
                
                
        ]);
        
        
        
        
        
        
        
        return $this->render('vista1', [
            "dataProvider" => $dataProvider,
            "title"=>"Noticias de la etiqueta",
            
            
          ]);
        
    }
    
    public function actionNoticiasfotografos($fotografo){
        $dataProvider = new ActiveDataProvider([
            'query'=>Fotografos::findOne($fotografo)->getNoticias(),
        ]);
        
        //var_dump(Fotografos::findOne($fotografo)->getNoticias());
        //exit;
        
        
        
        
        
        
        
        
        return $this->render('vista1', [
            "dataProvider" => $dataProvider,
            "title"=>"Noticias del Fotógrafo",
          ]);
        
    }
    
    public function actionAutorfotografos($fotografo){
        $dataProvider = new ActiveDataProvider([
            'query'=>Fotografos::findOne($fotografo)->getAutores(),
        ]);
        
               
        return $this->render('vista2', [
            "dataProvider" => $dataProvider,
            "titulo"=>"Autores del Fotógrafo",
          ]);
        
    }
    
    

    /**
     * Displays a single Noticias model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Noticias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Noticias();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Noticias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Noticias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Noticias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Noticias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Noticias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
