<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Noticias;
use app\models\Autores;
use app\models\Fotografos;
use app\models\Etiquetas;
use app\models\Categorizan;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays frontend page for noticias.
     *
     * 
     */
    public function actionIndexnoticias()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);

        return $this->render('vista1', [
            "dataProvider" => $dataProvider,
            "campos"=>['texto'],
            "titulo"=>"Noticias de nuestro periódico",
          ]);
    }
    
     /**
     * Displays frontend page for autores.
     *
     * 
     */
    public function actionIndexautores()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Autores::find(),
        ]);

        return $this->render('vista2', [
            "dataProvider" => $dataProvider,
            "campos"=>['texto'],
            "titulo"=>"Autores de nuestro periódico",
          ]);
    }
    
     /**
     * Displays frontend page for fotografos.
     *
     * 
     */
    public function actionIndexfotografos()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografos::find(),
        ]);

        return $this->render('vista3', [
            "dataProvider" => $dataProvider,
            "campos"=>['texto'],
            "titulo"=>"Fotógrafos de nuestro periódico",
          ]);
    }
    
     /**
     * Displays frontend page for fotografos.
     *
     * 
     */
    public function actionIndexetiquetas()
    {
       
        
        $numnoticias = Etiquetas::find()
                ->select("etiquetas.id,etiqueta,count(*) numeroNoticias,count(distinct idautor) numeroAutores")
                ->innerJoinWith("noticias")
                ->groupBy('etiquetas.id,etiqueta');
        
        
         $dataProvider = new ActiveDataProvider([
            'query' => $numnoticias,
        ]);
        
        
        //var_dump($numnoticias);
                
        

        return $this->render('vista4', [
            "dataProvider" => $dataProvider,
            "titulo"=>"Etiquetas de nuestro periódico",
            
          ]);
    }
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
